﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace CSCI411MainApp
{
    public class User
    {
        public User()
        {
            
        }

        // Users, Login, and Registration
        public int addUser(string firstName, string username, string PWD)
        {

            DataAccess myAccess = new DataAccess();
            string myQuery = "spAddNewUser";
            SqlParameter[] myParameters = new SqlParameter[3];
            myParameters[0] = new SqlParameter("firstName", firstName);
            myParameters[1] = new SqlParameter("username", username);
            myParameters[2] = new SqlParameter("password", encrypt(PWD));

            int rows = myAccess.nonQuery(myQuery, myParameters);

            return rows;
        }

        public int login(string username, string PWD)
        {

            DataAccess myAccess = new DataAccess();
            string myQuery = "spGetUser";
            SqlParameter[] myParameters = new SqlParameter[2];
            myParameters[0] = new SqlParameter("username", username);
            myParameters[1] = new SqlParameter("password", encrypt(PWD));

            int verify = myAccess.executeScalar(myQuery, myParameters);

            return verify;
        }

        public int checkExists(string username) {
            DataAccess myAccess = new DataAccess();
            string myQuery = "spCheckExists";
            SqlParameter[] myParameters = new SqlParameter[1];
            myParameters[0] = new SqlParameter("username", username);

            int verify = myAccess.executeScalar(myQuery, myParameters);

            return verify;
        }


        // Main Button Code
        public int setMainBtnStats(int timesClicked, string recentClicker, DateTime timeOfClick, string regionOfClick) {

            DataAccess myAccess = new DataAccess();
            string myQuery = "spAddMainBtnStats";
            SqlParameter[] myParameters = new SqlParameter[4];
            myParameters[0] = new SqlParameter("timesClicked", timesClicked);
            myParameters[1] = new SqlParameter("recentClicker", recentClicker);
            myParameters[2] = new SqlParameter("timeOfClick", timeOfClick);
            myParameters[3] = new SqlParameter("regionOfClick", regionOfClick);

            int rows = myAccess.nonQuery(myQuery, myParameters);

            return rows;
        }

        public int setUserMainBtnStats(int mainBtnClicks, string username) {

            DataAccess myAccess = new DataAccess();
            string myQuery = "spAddUserMainBtnStats";
            SqlParameter[] myParameters = new SqlParameter[2];
            myParameters[0] = new SqlParameter("userMainBtnClicks", mainBtnClicks);
            myParameters[1] = new SqlParameter("username", username);

            int rows = myAccess.nonQuery(myQuery, myParameters);

            return rows;
        }

        public int getMainBtnClicks() {

            int timesClicked = 0;
            DataAccess myAccess = new DataAccess();
            string myQuery = "spGetMainBtnClicks";
            timesClicked = Convert.ToInt32(myAccess.getQuery(myQuery).Tables[0].Rows[0][0]);

            return timesClicked;
        }

        public string getMainBtnRecentClicker() {
            string recentClicker = "";
            DataAccess myAccess = new DataAccess();
            string myQuery = "spGetMainBtnRecentClicker";
            recentClicker = Convert.ToString(myAccess.getQuery(myQuery).Tables[0].Rows[0][0]);

            return recentClicker;
        }

        public DateTime getMainBtnTimeOfClick() {
            DateTime timeOfRecentClick;
            DataAccess myAccess = new DataAccess();
            string myQuery = "spGetMainBtnTimeOfClick";
            timeOfRecentClick = Convert.ToDateTime(myAccess.getQuery(myQuery).Tables[0].Rows[0][0]);
    
            return timeOfRecentClick;
        }

        public int getUserMainBtnClicks()
        {

            int timesClicked = 0;
            DataAccess myAccess = new DataAccess();
            string myQuery = "spGetUserMainBtnClicks";
            timesClicked = Convert.ToInt32(myAccess.getQuery(myQuery).Tables[0].Rows[0][0]);

            return timesClicked;
        }

        public int getUserMainBtnClicks(string username)
        {
            DataAccess myAccess = new DataAccess();
            string myQuery = "spGetUserMainBtnClicks";
            SqlParameter[] myParameters = new SqlParameter[1];
            myParameters[0] = new SqlParameter("username", username);

            int timesClicked = Convert.ToInt32(myAccess.getQuery(myQuery, myParameters).Tables[0].Rows[0][0]);

            return timesClicked;
        }

        private Byte[] encrypt(string unencryptedString)
        {
            // encrypt password before inserted..
            Byte[] hashedDataBytes = null;
            UTF8Encoding encoder = new UTF8Encoding();

            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            hashedDataBytes = md5Hasher.ComputeHash(encoder.GetBytes(unencryptedString));

            return hashedDataBytes;
        }


    }
}