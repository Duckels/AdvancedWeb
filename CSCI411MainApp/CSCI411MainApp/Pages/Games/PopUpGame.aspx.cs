﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSCI411MainApp
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        Random random = new Random();
        string[] cssClasses = {"btnPos1", "btnPos2", "btnPos3", "btnPos4", "btnPos5", "btnPos6", "btnPos7", "btnPos8", "btnPos9", "btnPos10", };
        int buttonsLeft;

        protected void Page_Load(object sender, EventArgs e)
        {
            buttonsLeft = 5;
        }

        protected void timerTick(object sender, EventArgs e)
        {
            btnInside1 = generateRandomBtns(btnInside1);
            btnInside2 = generateRandomBtns(btnInside2);
            btnInside3 = generateRandomBtns(btnInside3);
            btnInside4 = generateRandomBtns(btnInside4);
            btnInside5 = generateRandomBtns(btnInside5);
       
        }

        protected Button generateRandomBtns(Button btn)
        {
            int randClass = random.Next(0, 10);
            btn.CssClass = cssClasses[randClass];

            return btn;
        }

        protected void btnInside1_Click(object sender, EventArgs e)
        {
            btnInside1.Visible = false;
            buttonsLeft -= 1;
            Label1.Text = "Buttons Remaining: " + buttonsLeft;
            if (buttonsLeft == 0)
            {
                Panel1.Visible = false;
            }
        }
        protected void btnInside2_Click(object sender, EventArgs e)
        {
            btnInside2.Visible = false;
            buttonsLeft -= 1;
            Label1.Text = "Buttons Remaining: " + buttonsLeft;
            if (buttonsLeft == 0)
            {
                Panel1.Visible = false;
            }
        }
        protected void btnInside3_Click(object sender, EventArgs e)
        {
            btnInside3.Visible = false;
            buttonsLeft -= 1;
            Label1.Text = "Buttons Remaining: " + buttonsLeft;
            if (buttonsLeft == 0)
            {
                Panel1.Visible = false;
            }
        }
        protected void btnInside4_Click(object sender, EventArgs e)
        {
            btnInside4.Visible = false;
            buttonsLeft -= 1;
            Label1.Text = "Buttons Remaining: " + buttonsLeft;
            if (buttonsLeft == 0)
            {
                Panel1.Visible = false;
            }
        }
        protected void btnInside5_Click(object sender, EventArgs e)
        {
            btnInside5.Visible = false;
            buttonsLeft -= 1;
            Label1.Text = "Buttons Remaining: " + buttonsLeft;
            if (buttonsLeft == 0)
            {
                Panel1.Visible = false;
            }
        }
    }
}