﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopUpGame.aspx.cs" Inherits="CSCI411MainApp.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="Site.css"/>
    <title></title>
</head>
<body>

     <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <div>
            <asp:Timer ID="Timer1" OnTick="timerTick" runat="server" Interval="10000"> </asp:Timer>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>

            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server" CssClass="popUpPanel" >
        
                    <asp:Button ID="btnInside1" CssClass="btnPos1" OnClick="btnInside1_Click" runat="server" Text="Here!" />
                    <asp:Button ID="btnInside2" CssClass="btnPos4" OnClick="btnInside2_Click" runat="server" Text="Here!" />
                    <asp:Button ID="btnInside3" CssClass="btnPos10" OnClick="btnInside3_Click" runat="server" Text="Here!" />
                    <asp:Button ID="btnInside4" CssClass="btnPos3" OnClick="btnInside4_Click" runat="server" Text="Here!" />
                    <asp:Button ID="btnInside5" CssClass="btnPos6" OnClick="btnInside5_Click" runat="server" Text="Here!" /> 
                    <asp:Label ID="Label1" runat="server" Text="Buttons Remaining: 5"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
        
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">

            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
            
            <ContentTemplate>
                


            </ContentTemplate>
        
        </asp:UpdatePanel>

    </form>
</body>
</html>
