﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSCI411MainApp
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null)
            {
                lblWelcome.Text = "Welcome: " + Session["username"];
            } else {
                lblWelcome.Text = "Welcome to The Game of Buttons";
            }

        }
    }
}