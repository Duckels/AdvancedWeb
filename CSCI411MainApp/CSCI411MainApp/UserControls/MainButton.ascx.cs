﻿using CSCI411MainApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSCI411MainApp.UserControls
{
    public partial class MainButton : System.Web.UI.UserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get main button stats
            User myUser = new User();
            int timesClicked = myUser.getMainBtnClicks();
            string recentClicker = myUser.getMainBtnRecentClicker();
            DateTime timeOfClick = myUser.getMainBtnTimeOfClick();
            string regionOfRecentClick = "US";

            // Display Main Button Stats
            lblTimesClicked.Text = "Times Clicked: " + timesClicked.ToString();
            lblRecentClicker.Text = "Most Recent Clicker: " + recentClicker;
            lblTimeOfClick.Text = "Time of Most Recent Click: " + timeOfClick;
            lblRegionClick.Text = "Region of Most Recent Clicker: " + regionOfRecentClick;

            // Get and Display User Data
            if (Session["username"] != null)
            {
                int userMainBtnClicks = myUser.getUserMainBtnClicks(Session["username"].ToString());
                lblUserData.Visible = true;
                lblMainBtnClicks.Visible = true;
                lblMainBtnClicks.Text = Session["username"] + "'s Main Button Clicks: " + userMainBtnClicks;

                //  Button Config
                if (userMainBtnClicks <= 250 && userMainBtnClicks > 0)
                {
                    mainBtn.CssClass = "roundButtonBronze";

                }
                else if (userMainBtnClicks <= 500 && userMainBtnClicks > 250)
                {
                    mainBtn.CssClass = "roundButtonSilver";

                }
                else if (userMainBtnClicks <= 750 && userMainBtnClicks > 500)
                {
                    mainBtn.CssClass = "roundButtonGold";

                }
                else if (userMainBtnClicks > 750)
                {
                    mainBtnImage.Visible = true;
                    mainBtn.Visible = false;

                }
            }
        }

        protected void mainBtn_Click(object sender, EventArgs e)
        {
            // Get main button stats
            User myUser = new User();
            int timesClicked = myUser.getMainBtnClicks();
            string recentClicker = myUser.getMainBtnRecentClicker();
            DateTime timeOfClick = myUser.getMainBtnTimeOfClick();
            string regionOfRecentClick = "US";

            // Determine user or guest
            if (Session["username"] == null) {
                recentClicker = "Guest";
            }
            else {
                recentClicker = Session["username"].ToString();
            }

            int userMainBtnClicks;
            // Get and Update User Main Button Stats
            if (Session["username"] != null) {
                userMainBtnClicks = myUser.getUserMainBtnClicks(Session["username"].ToString());
                userMainBtnClicks = userMainBtnClicks + 1;
                myUser.setUserMainBtnStats(userMainBtnClicks, Session["username"].ToString());
                lblMainBtnClicks.Text = Session["username"] + "'s Main Button Clicks: " + userMainBtnClicks;
            }

            // Set stats
            timesClicked = timesClicked + 1;
            timeOfClick = DateTime.Now;

            // Update main button stats and user main button stats
            myUser.setMainBtnStats(timesClicked, recentClicker, timeOfClick, "US");

            // Display main button stats
            lblTimesClicked.Text = "Times Clicked: " + timesClicked.ToString();
            lblRecentClicker.Text = "Most Recent Clicker: " + recentClicker;
            lblTimeOfClick.Text = "Time of Most Recent Click: " + timeOfClick;
            lblRegionClick.Text = "Region of Most Recent Clicker: " + regionOfRecentClick;

        }
    }
}