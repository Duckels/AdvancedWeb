﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainButton.ascx.cs" Inherits="CSCI411MainApp.UserControls.MainButton" %>

    <script type="text/javascript">
        // plain JavaScript example
        function jsonpCallback(data) { 
        alert('Latitude: ' + data.latitude + 
              '\nLongitude: ' + data.longitude + 
              '\nCountry: ' + data.address.country); 
        }
    </script>

<asp:ScriptManager ID="mainBtnManager" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="mainBtnUpdatePanel" runat="server">
    
    <ContentTemplate>
        <asp:Button ID="mainBtn" runat="server" CssClass="roundButton" Text="Main" OnClick="mainBtn_Click" /> <br /> <br />
        <asp:ImageButton ID="mainBtnImage" Visible="false" ImageUrl="~/mountain.jpg" runat="server" CssClass="roundButtonGold" OnClick="mainBtn_Click" /> <br /> <br />

        <asp:Label ID="lblGeneral" runat="server" Text="General Data"></asp:Label><br />
        <asp:Label ID="lblTimesClicked" runat="server" Text="Times Clicked: "></asp:Label> <br />
        <asp:Label ID="lblRecentClicker" runat="server" Text="Most Recent Clicker: "></asp:Label> <br />
        <asp:Label ID="lblRegionClick" runat="server" Text="Region of Most Recent Clicker: "></asp:Label> <br />
        <asp:Label ID="lblTimeOfClick" runat="server" Text="Time of Most Recent Click: "></asp:Label> <br />
    </ContentTemplate>

</asp:UpdatePanel> 
<br />

<asp:Label ID="lblUserData" Visible="false" runat="server" Text="User Data"></asp:Label><br />
<asp:Label ID="lblMainBtnClicks" Visible="false" runat="server" Text="Main Button Clicks: "></asp:Label> <br />
<asp:Label ID="lblOtherBtnClicks" Visible="false" runat="server" Text="Other Button Clicks: "></asp:Label> <br />
