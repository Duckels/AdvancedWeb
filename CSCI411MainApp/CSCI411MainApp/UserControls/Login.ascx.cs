﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSCI411MainApp.UserControls
{
    public partial class Login : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null) {
                btnLogout.Visible = true;
                loginPanel.Visible = false;
            } else {
                loginPanel.Visible = true;
                btnLogout.Visible = false;
            }
       
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if (registerPanel.Visible == false) {
                registerPanel.Visible = true;
            } else if (registerPanel.Visible == true){
                registerPanel.Visible = false;
            }
        }

        protected void addUserBtn_Click(object sender, EventArgs e)
        {
            if (txtRegisterUsername.Text != "" && txtRegisterPWD != null) {
                string firstName = txtRegisterName.Text;
                string username = txtRegisterUsername.Text;
                string PWD = txtRegisterPWD.Text;

                User myUser = new User();
                if (myUser.checkExists(username) == 1) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('User Already Exists'); window.open(Home.aspx);", true);
                } else {
                    myUser.addUser(firstName, username, PWD);
                }

                txtRegisterName.Text = "";
                txtRegisterUsername.Text = "";
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string userInputUsername = txtUsername.Text;
            string userInputPWD = txtPassword.Text;

            User myUser = new User();
            
            if (myUser.login(userInputUsername,userInputPWD) == 0) {
                Session["username"] = "";
            } else if (myUser.login(userInputUsername, userInputPWD) == 1) {
                Session["username"] = userInputUsername;
                Session["PWD"] = userInputPWD;
                Server.Transfer("Home.aspx");
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["username"] = null;
            Session["PWD"] = null;
            loginPanel.Visible = true;
            btnLogout.Visible = false;
            Server.Transfer("Home.aspx");
        }
    }
}