﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="navigation.ascx.cs" Inherits="CSCI411MainApp.navigation" %>

<!-- Pull user information about button clicks to determine enabled for links.
     
-->

<asp:Panel Visible="false" ID="navPanel" runat="server">

 <asp:Menu ID="Menu1" CssClass="left" runat="server" Orientation="Vertical">
    <Items>
        <asp:MenuItem NavigateUrl="~/Pages/Home.aspx" Text="Home" Value="Home"></asp:MenuItem>
        <asp:MenuItem Text="Games" Value="Games">
            <asp:MenuItem NavigateUrl="~/Pages/Games/PopUpGame.aspx" Text="Pop Up Game"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Pages/Games/KingOfHill.aspx" Text="King Of The Hill"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Pages/Games/WebForm1.aspx" Text="Space Invaders" Enabled="false"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Pages/Games/WebForm1.aspx" Text="Simple Shooter" Enabled="false"></asp:MenuItem>
        </asp:MenuItem>
    </Items>
</asp:Menu>

</asp:Panel>





