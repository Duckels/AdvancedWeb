﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="CSCI411MainApp.UserControls.Login" %>

<asp:Panel ID="loginPanel" CssClass="Login" runat="server" Width="209px">
    <h2>Login</h2>
    <asp:Label ID="lblUsername" runat="server" Text="Username:"></asp:Label>
    <br />
    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
    <br />
    <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label>
    <br />
    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
    <br />
    <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click"/>
    <asp:Button ID="btnRegister" runat="server" Text="Register" OnClick="btnRegister_Click" />
    
</asp:Panel>

<asp:Button ID="btnLogout" CssClass="logoutBtn" Visible="false" runat="server" Text="Log Out" OnClick="btnLogout_Click" />

<asp:Panel ID="registerPanel" runat="server" Visible="false" Width="330px" CssClass="Register">
    <h2>Register</h2>
    Name:<br />
    <asp:TextBox ID="txtRegisterName" runat="server"></asp:TextBox>
    <br />
    Username:<br />
    <asp:TextBox ID="txtRegisterUsername" runat="server"></asp:TextBox>
    <br />
    Password:<br />
    <asp:TextBox ID="txtRegisterPWD" runat="server" TextMode="Password"></asp:TextBox>
    <br />
    <asp:Button ID="addUserBtn" runat="server" Text="Submit" OnClick="addUserBtn_Click"/>
</asp:Panel>


