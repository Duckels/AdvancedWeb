﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSCI411MainApp
{
    public partial class navigation : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null) {
                navPanel.Visible = true;
            }else if (Session["username"] == null) {
                navPanel.Visible = false;
            }
        }
    }
}